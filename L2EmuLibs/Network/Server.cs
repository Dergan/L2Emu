﻿using L2EmuLibs.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace L2EmuLibs.Network
{
    public abstract class Server
    {
        public abstract Peer GetNewClient(Socket socket);
        public abstract void onNewClient(Peer peer);

        internal Socket TcpServer { get; private set; }
        internal SortedList<decimal, Peer> Clients { get; private set; }
        private RandomDecimal randomDecimal = new RandomDecimal(DateTime.Now.Millisecond);

        public Server(string ListenIp, int ListenPort)
        {
            this.Clients = new SortedList<decimal, Peer>();
            this.TcpServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.TcpServer.Bind(new IPEndPoint(IPAddress.Parse(ListenIp), ListenPort));
            this.TcpServer.Listen(100);
            this.TcpServer.BeginAccept(AsyncAction, null);
        }

        private void AsyncAction(IAsyncResult result)
        {
            try
            {
                Socket AcceptSocket = this.TcpServer.EndAccept(result); //<- can throw a error
                Peer client = GetNewClient(AcceptSocket);
                client.Server = this;
                client.ClientId = randomDecimal.NextDecimal();

                lock (Clients)
                {
                    while (Clients.ContainsKey(client.ClientId))
                        client.ClientId = randomDecimal.NextDecimal();
                    Clients.Add(client.ClientId, client);
                }
                onNewClient(client);
            }
            catch { }
            this.TcpServer.BeginAccept(AsyncAction, null);
        }

        public void Dispose()
        {
            TcpServer.Close();

            lock (Clients)
            {
                foreach (Peer peer in Clients.Values)
                    peer.Disconnect();
            }
        }
    }
}