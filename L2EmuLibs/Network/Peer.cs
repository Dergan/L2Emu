﻿using L2EmuLibs.ByteX;
using L2EmuLibs.Utils;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace L2EmuLibs.Network
{
    public abstract class Peer
    {
        public const int HEADER_SIZE = 2;
        public Socket Handle { get; private set; }
        public string RemoteIp { get; private set; }

        //connection info
        public ulong PacketsIn { get; private set; }
        public ulong DataIn { get; private set; }
        public ulong PacketsOut { get; private set; }
        public ulong DataOut { get; private set; }

        //header info
        private int PayloadLen = 0;
        private byte[] Buffer = new byte[8192];

        private ReceiveType ReceiveState = ReceiveType.Header;

        //receive info
        private int ReadOffset { get; set; }
        private int WriteOffset { get; set; }
        private int ReadableDataLen { get; set; }

        public bool Connected { get; private set; }
        public Server Server { get; internal set; }
        public decimal ClientId { get; internal set; }

        private object SendLock = new object();

        public Peer(Socket Handle)
        {
            this.Handle = Handle;
            try
            {
                this.RemoteIp = this.Handle.RemoteEndPoint.ToString().Split(':')[0];
            }
            catch
            {
                
            }
            onConnect();
            Handle.BeginReceive(this.Buffer, 0, this.Buffer.Length, SocketFlags.None, AynsReceive, null);
        }

        protected abstract void onReceivePayload(byte[] Data);
        protected abstract void onConnect();
        protected abstract void onDisconnect();
        protected abstract void onSendPacket(ref byte[] Payload, Bytex byteX);

        private void AynsReceive(IAsyncResult result)
        {
            int BytesTransferred = Handle.EndReceive(result);
            if (BytesTransferred <= 0)
            {
                onDisconnect();
                this.Connected = false;
                return;
            }

            ReadableDataLen += BytesTransferred;
            DataIn += (ulong)BytesTransferred;
            bool Process = true;

            while (Process)
            {
                if (ReceiveState == ReceiveType.Header)
                {
                    Process = ReadableDataLen >= HEADER_SIZE;
                    if (ReadableDataLen >= HEADER_SIZE)
                    {
                        PayloadLen = BitConverter.ToUInt16(Buffer, ReadOffset);
                        PayloadLen -= 2; //already read 2 bytes

                        ReadableDataLen -= HEADER_SIZE;
                        ReadOffset += HEADER_SIZE;
                        ReceiveState = ReceiveType.Payload;
                    }
                }
                else if (ReceiveState == ReceiveType.Payload)
                {
                    Process = ReadableDataLen >= PayloadLen;
                    if (ReadableDataLen >= PayloadLen)
                    {
                        bool ProcessPacket = true;

                        if (ProcessPacket)
                        {
                            using (PayloadReader pr = new PayloadReader(this.Buffer))
                            {
                                pr.Offset = ReadOffset;
                                onReceivePayload(pr.ReadBytes(PayloadLen));
                            }
                        }

                        PacketsIn++;
                        ReadOffset += PayloadLen;
                        ReadableDataLen -= PayloadLen;
                        ReceiveState = ReceiveType.Header;
                    }
                }
            }

            int len = ReceiveState == ReceiveType.Header ? HEADER_SIZE : PayloadLen;
            if (ReadOffset + len >= this.Buffer.Length)
            {
                //no more room for this data size, at the end of the buffer ?

                //copy the buffer to the beginning
                Array.Copy(this.Buffer, ReadOffset, this.Buffer, 0, ReadableDataLen);

                WriteOffset = ReadableDataLen;
                ReadOffset = 0;
            }
            else
            {
                //payload fits in the buffer from the current offset
                //use BytesTransferred to write at the end of the payload
                //so that the data is not split
                WriteOffset += BytesTransferred;
            }

            try
            {
                Handle.BeginReceive(this.Buffer, WriteOffset, Buffer.Length - WriteOffset, SocketFlags.None, AynsReceive, null);
            }
            catch
            {
                //client most likely disconnected
                onDisconnect();
            }
        }

        public void SendPacket(Bytex byteX)
        {
            lock (SendLock)
            {
                using (PayloadWriter pw = new PayloadWriter())
                {
                    byteX.onSend();
                    byte[] payload = byteX.Serialize(byteX);

                    onSendPacket(ref payload, byteX);

                    pw.WriteShort((short)(payload.Length + 2));
                    pw.WriteBytes(payload);

                    try
                    {
                        Handle.Send(pw.ToByteArray());
                    }
                    catch
                    {

                    }
                }
            }
        }

        public void Disconnect()
        {
            try
            {
                Handle.Close();
            }
            catch { }
        }
    }
}