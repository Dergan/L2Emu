﻿using L2EmuLibs.ByteX;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace L2EmuLibs.Network
{
    public class PacketHandler
    {
        private SortedList<int, Type> types;

        /// <summary>
        /// Initialize the Packet Handler
        /// </summary>
        /// <param name="NameSpace">The namespace of where to get the Receive Packet Types from</param>
        public PacketHandler(string NameSpace)
        {
            this.types = new SortedList<int, Type>();

            foreach (Type type in Assembly.GetCallingAssembly().GetTypes())
            {
                if (type.BaseType == null)
                    continue;

                if (type.FullName.ToLower().StartsWith(NameSpace.ToLower()) && type.BaseType == typeof(Bytex))
                {
                    Bytex temp = (Bytex)Activator.CreateInstance(type);
                    int packetId = temp.GetPacketId(temp);

                    types.Add(packetId, type);
                }
            }
        }

        public Bytex GetPacket(int PacketId)
        {
            Type type = null;
            if(types.TryGetValue(PacketId, out type))
            {
                return (Bytex)Activator.CreateInstance(type);
            }
            return null;
        }
    }
}