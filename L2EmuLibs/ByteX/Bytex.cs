﻿using L2EmuLibs.ByteX.Attributes;
using L2EmuLibs.Network;
using L2EmuLibs.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace L2EmuLibs.ByteX
{
    public abstract class Bytex
    {
        public abstract void onReceive();
        public abstract void onSend();

        public Peer Peer { get; set; }

        public Bytex()
        {

        }

        public byte[] Serialize(Bytex byteX)
        {
            PayloadWriter pw = new PayloadWriter();

            if (byteX.GetType().GetCustomAttributes(typeof(XNoPacketIdAttribute), false).Length == 0)
            {
                //first write the Packet Id
                bool FoundPacketId = false;
                foreach (PropertyInfo inf in byteX.GetType().GetProperties())
                {
                    object[] temp = null;

                    if ((temp = inf.GetCustomAttributes(typeof(XPacketIdAttribute), false)).Length > 0)
                    {
                        FoundPacketId = true;

                        XPacketIdAttribute byteAttr = temp[0] as XPacketIdAttribute;
                        if (byteAttr.SingleByte)
                            pw.WriteByte(byteAttr.PacketId);
                        else
                            pw.WriteBytes(byteAttr.PacketArrId);
                    }
                }
                if (!FoundPacketId)
                {
                    throw new Exception("No packet id was found in this packed");
                }
            }

            foreach (PropertyInfo inf in byteX.GetType().GetProperties())
            {
                object[] temp = null;

                if ((temp = inf.GetCustomAttributes(typeof(XByteAttribute), false)).Length > 0)
                {
                    XByteAttribute byteAttr = temp[0] as XByteAttribute;

                    if (byteAttr.UseConstant)
                    {
                        pw.WriteByte(byteAttr.ConstValue);
                    }
                    else
                    {
                        pw.WriteByte((byte)inf.GetValue(byteX, null));
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XByteArrayAttribute), false)).Length > 0)
                {
                    XByteArrayAttribute byteArrAttr = temp[0] as XByteArrayAttribute;
                    if (byteArrAttr.UseConstant)
                    {
                        if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue == null)
                            throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, Value=NULL");
                        if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue.Length != byteArrAttr.SizeCheck)
                            throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, ValueLength=" + byteArrAttr.ConstValue.Length + ", SizeCheck=" + byteArrAttr.SizeCheck);

                        pw.WriteBytes(byteArrAttr.ConstValue);
                    }
                    else
                    {
                        if (byteArrAttr.SizeCheck >= 0 && inf.GetValue(byteX, null) == null)
                            throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, Value=NULL");

                        byte[] byteArr = (byte[])inf.GetValue(byteX, null);
                        if (byteArrAttr.SizeCheck >= 0 && byteArr.Length != byteArrAttr.SizeCheck)
                            throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, ValueLength=" + byteArrAttr.ConstValue.Length + ", SizeCheck=" + byteArrAttr.SizeCheck);

                        pw.WriteBytes(byteArr);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XShortAttribute), false)).Length > 0)
                {
                    XShortAttribute intAttr = temp[0] as XShortAttribute;
                    if (intAttr.UseConstant)
                    {
                        pw.WriteShort(intAttr.ConstValue);
                    }
                    else
                    {
                        pw.WriteShort((short)inf.GetValue(byteX, null));
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XIntegerAttribute), false)).Length > 0)
                {
                    XIntegerAttribute intAttr = temp[0] as XIntegerAttribute;
                    if (intAttr.UseConstant)
                    {
                        pw.WriteInteger(intAttr.ConstValue);
                    }
                    else
                    {
                        pw.WriteInteger((int)inf.GetValue(byteX, null));
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XStringAttribute), false)).Length > 0)
                {
                    XStringAttribute strAttr = temp[0] as XStringAttribute;
                    if (strAttr.UseConstant)
                    {
                        pw.WriteString(strAttr.ConstValue);
                    }
                    else
                    {
                        pw.WriteString((string)inf.GetValue(byteX, null));
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XListAttribute), false)).Length > 0)
                {
                    XListAttribute listAttr = temp[0] as XListAttribute;

                    ICollection collection = (ICollection)inf.GetValue(byteX, null);
                    IEnumerator Enumerator = collection.GetEnumerator();

                    while(Enumerator.MoveNext())
                    {
                        Bytex b = (Bytex)Enumerator.Current;
                        pw.WriteBytes(b.Serialize(b));
                    }
                }
            }

            return pw.ToByteArray();
        }

        public void Deserialize(byte[] Data, Bytex packet)
        {
            PayloadReader pr = new PayloadReader(Data);
            foreach (PropertyInfo inf in packet.GetType().GetProperties())
            {
                object[] temp = null;



                if ((temp = inf.GetCustomAttributes(typeof(XPacketIdAttribute), false)).Length > 0)
                {
                    XPacketIdAttribute byteAttr = temp[0] as XPacketIdAttribute;

                    if (byteAttr.SingleByte)
                    {
                        inf.SetValue(packet, byteAttr.PacketId, null);
                    }
                    else
                    {
                        inf.SetValue(packet, byteAttr.PacketArrId, null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XByteAttribute), false)).Length > 0)
                {
                    XByteAttribute byteAttr = temp[0] as XByteAttribute;

                    if (byteAttr.UseConstant)
                    {
                        inf.SetValue(packet, byteAttr.ConstValue, null);
                    }
                    else
                    {
                        inf.SetValue(packet, pr.ReadByte(), null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XByteArrayAttribute), false)).Length > 0)
                {
                    XByteArrayAttribute byteArrAttr = temp[0] as XByteArrayAttribute;
                    if (byteArrAttr.UseConstant)
                    {
                        if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue == null)
                            throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, Value=NULL");
                        if (byteArrAttr.SizeCheck >= 0 && byteArrAttr.ConstValue.Length != byteArrAttr.SizeCheck)
                            throw new ArgumentNullException("The Size must be equal to the Byte Array's Length, ValueLength=" + byteArrAttr.ConstValue.Length + ", SizeCheck=" + byteArrAttr.SizeCheck);

                        inf.SetValue(packet, byteArrAttr.ConstValue, null);
                    }
                    else
                    {
                        if (byteArrAttr.UnknownSize)
                        {
                            inf.SetValue(packet, pr.ReadBytes(pr.Packet.Length - pr.Offset), null);
                            break;
                        }

                        if (byteArrAttr.SizeCheck <= 0)
                            throw new Exception("Byte Array Length must be >=0");

                        inf.SetValue(packet, pr.ReadBytes(byteArrAttr.SizeCheck), null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XShortAttribute), false)).Length > 0)
                {
                    XShortAttribute intAttr = temp[0] as XShortAttribute;
                    if (intAttr.UseConstant)
                    {
                        inf.SetValue(packet, intAttr.ConstValue, null);
                    }
                    else
                    {
                        inf.SetValue(packet, pr.ReadShort(), null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XIntegerAttribute), false)).Length > 0)
                {
                    XIntegerAttribute intAttr = temp[0] as XIntegerAttribute;
                    if (intAttr.UseConstant)
                    {
                        inf.SetValue(packet, intAttr.ConstValue, null);
                    }
                    else
                    {
                        inf.SetValue(packet, pr.ReadInteger(), null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XStringAttribute), false)).Length > 0)
                {
                    XStringAttribute strAttr = temp[0] as XStringAttribute;
                    if (strAttr.UseConstant)
                    {
                        inf.SetValue(packet, strAttr.ConstValue, null);
                    }
                    else
                    {
                        inf.SetValue(packet, pr.ReadString(), null);
                    }
                }
            }
        }

        public int GetPacketId(Bytex byteX)
        {
            foreach (PropertyInfo inf in byteX.GetType().GetProperties())
            {
                object[] temp = null;

                if ((temp = inf.GetCustomAttributes(typeof(XPacketIdAttribute), false)).Length > 0)
                {
                    XPacketIdAttribute byteAttr = temp[0] as XPacketIdAttribute;
                    if (byteAttr.SingleByte)
                    {
                        return byteAttr.PacketId;
                    }
                    else
                    {
                        if (byteAttr.PacketArrId.Length == 1)
                            return byteAttr.PacketArrId[0];
                        else if (byteAttr.PacketArrId.Length == 2)
                            return BitConverter.ToUInt16(byteAttr.PacketArrId, 0);
                        throw new Exception("No support for Byte Arrays with a length of " + byteAttr.PacketArrId.Length);
                    }
                }
            }
            throw new Exception("No packet id was found in this packed");
        }

        public SortedList<string, string> GetObjectStrings(Bytex byteX)
        {
            SortedList<string, string> TempList = new SortedList<string, string>();

            foreach (PropertyInfo inf in byteX.GetType().GetProperties())
            {
                string val = "";

                object[] temp = null;

                if ((temp = inf.GetCustomAttributes(typeof(XByteAttribute), false)).Length > 0)
                {
                    XByteAttribute byteAttr = temp[0] as XByteAttribute;

                    if (byteAttr.UseConstant)
                    {
                        val = "[Const] Value=" + byteAttr.ConstValue.ToString("X2");
                    }
                    else
                    {
                        val = "Value=" + ((byte)inf.GetValue(byteX, null)).ToString("X2");
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XByteArrayAttribute), false)).Length > 0)
                {
                    XByteArrayAttribute byteArrAttr = temp[0] as XByteArrayAttribute;
                    if (byteArrAttr.UseConstant)
                    {
                        val = "[Const] Value=" + BitConverter.ToString(byteArrAttr.ConstValue).Replace('-', ' ');
                    }
                    else
                    {
                        if ((byte[])inf.GetValue(byteX, null) == null)
                            val = "Value=NULL";
                        else
                            val = "Value=" + BitConverter.ToString((byte[])inf.GetValue(byteX, null)).Replace('-', ' ');
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XShortAttribute), false)).Length > 0)
                {
                    XShortAttribute intAttr = temp[0] as XShortAttribute;
                    if (intAttr.UseConstant)
                    {
                        val = "[Const] Value=" + intAttr.ConstValue;
                    }
                    else
                    {
                        val = "Value=" + (short)inf.GetValue(byteX, null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XIntegerAttribute), false)).Length > 0)
                {
                    XIntegerAttribute intAttr = temp[0] as XIntegerAttribute;
                    if (intAttr.UseConstant)
                    {
                        val = "[Const] Value=" + intAttr.ConstValue;
                    }
                    else
                    {
                        val = "Value=" + (int)inf.GetValue(byteX, null);
                    }
                }
                else if ((temp = inf.GetCustomAttributes(typeof(XStringAttribute), false)).Length > 0)
                {
                    XStringAttribute strAttr = temp[0] as XStringAttribute;
                    if (strAttr.UseConstant)
                    {
                        val = "[Const] Value=" + strAttr.ConstValue;
                    }
                    else
                    {
                        val = "Value=" + inf.GetValue(byteX, null);
                    }
                }
                /*else if ((temp = inf.GetCustomAttributes(typeof(XListAttribute), false)).Length > 0)
                {
                    XListAttribute listAttr = temp[0] as XListAttribute;

                    ICollection collection = (ICollection)inf.GetValue(byteX, null);
                    IEnumerator Enumerator = collection.GetEnumerator();

                    string TempVal = "";

                    while (Enumerator.MoveNext())
                    {
                        Bytex b = (Bytex)Enumerator.Current;
                        SortedList<string, string> tempVals = GetObjectStrings(b);

                        
                    }
                }*/
                else if ((temp = inf.GetCustomAttributes(typeof(XPacketIdAttribute), false)).Length > 0)
                {
                    XPacketIdAttribute byteAttr = temp[0] as XPacketIdAttribute;
                    if (byteAttr.SingleByte)
                    {
                        if (byteAttr.SingleByte)
                            val = "[Const] Value=" + byteAttr.PacketId.ToString("X2");
                        else
                            val = "[Const] Value=" + BitConverter.ToString(byteAttr.PacketArrId).Replace('-', ' ');
                    }
                    else
                    {
                        val = "Value=" + inf.GetValue(byteX, null);
                    }
                }

                if (val.Length > 0)
                {
                    TempList.Add(inf.Name, val);
                }
            }
            return TempList;
        }
    }
}