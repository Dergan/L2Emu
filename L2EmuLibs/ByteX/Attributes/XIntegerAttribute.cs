﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2EmuLibs.ByteX.Attributes
{
    public class XIntegerAttribute : Attribute
    {
        public bool UseConstant { get; private set; }
        public int ConstValue { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        public XIntegerAttribute(int ConstValue)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
        }

        public XIntegerAttribute()
        {

        }
    }
}
