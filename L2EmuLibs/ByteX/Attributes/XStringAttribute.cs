﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2EmuLibs.ByteX.Attributes
{
    public class XStringAttribute : Attribute
    {
        public bool UseConstant { get; private set; }
        public string ConstValue { get; private set; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConstValue">The constant value used for the byte array if constant</param>
        public XStringAttribute(string ConstValue)
        {
            this.ConstValue = ConstValue;
            this.UseConstant = true;
        }

        public XStringAttribute()
        {

        }
    }
}