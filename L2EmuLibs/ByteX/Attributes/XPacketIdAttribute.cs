﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2EmuLibs.ByteX.Attributes
{
    public class XPacketIdAttribute : Attribute
    {
        public bool SingleByte { get; private set; }
        public byte PacketId { get; private set; }
        public byte[] PacketArrId { get; private set; }

        public XPacketIdAttribute(byte PacketId)
        {
            this.PacketId = PacketId;
            this.SingleByte = true;
        }

        public XPacketIdAttribute(byte[] PacketId)
        {
            this.PacketArrId = PacketId;
        }
    }
}