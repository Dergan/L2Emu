﻿using System;
using System.Collections.Generic;
using System.Text;

namespace L2EmuLibs.Utils
{
    public enum ReceiveType
    {
        Header,
        Payload
    }
}
