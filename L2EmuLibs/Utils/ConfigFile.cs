﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace L2EmuLibs.Utils
{
    public class ConfigFile
    {
        private System.IO.FileInfo File;
        private SortedList<string, SortedList<string, string>> _topics;
        public ConfigFile(string Path)
        {
            File = new System.IO.FileInfo(Path);
            _topics = new SortedList<string, SortedList<string, string>>();
            Reload();
        }

        public void Reload()
        {
            if (!File.Exists)
            {
                Logger.WriteLog(File.Name + File.Extension + " is not found!", Logger.LogType.Error);
                return;
            }
            StreamReader SR = new StreamReader(File.FullName);
            string curTopic = "";
            while (!SR.EndOfStream)
            {
                string line = SR.ReadLine();
                if (line.Length == 0)
                    continue;

                //This is a Comment.. skip it.
                if (line.StartsWith(";"))
                    continue;

                //This is a beginning of a topic...
                if (line.StartsWith("["))
                {
                    curTopic = line.Replace("[", "").Replace("]", "");
                    Logger.WriteLog("Topic added: " + curTopic, Logger.LogType.Initialize);

                    _topics.Add(curTopic, new SortedList<string, string>());
                    continue;
                }
                Logger.WriteLog("Prop added: " + line.Trim(), Logger.LogType.Initialize);
                string value = "";
                for (int i = 1; i < line.Trim().Split('=').Length; i++)
                {
                    if (i == line.Trim().Split('=').Length - 1)
                    {
                        value += line.Trim().Split('=')[i];
                    }
                    else
                    {
                        value += line.Trim().Split('=')[i] + "=";
                    }
                }
                _topics[curTopic].Add(line.Trim().Split('=')[0], value);
            }
        }

        public string GetProperty(string Topic, string Prop, string DefaultValue)
        {
            string ret = null;
            try
            {
                ret = _topics[Topic][Prop];
            }
            catch
            {
                ret = null;
            }
            return (ret == null ? DefaultValue : ret);
        }
    }
}