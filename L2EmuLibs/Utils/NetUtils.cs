﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace L2EmuLibs.Utils
{
    public class NetUtils
    {
        public static bool CheckIsLocal(byte[] IP)
        {
            if ((IP[0] == 127) && (IP[1] == 0) && (IP[2] == 0) && (IP[3] == 1))
                return true;

            byte[] localIP = Dns.GetHostByName(Dns.GetHostName()).AddressList[0].GetAddressBytes();
            if ((IP[0] == localIP[0]) && (IP[1] == localIP[1]) && (IP[2] == localIP[2]))
                return true;
            return false;
        }
    }
}