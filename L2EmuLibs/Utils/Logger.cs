﻿using L2EmuLibs.ByteX;
using System;
using System.Collections.Generic;
using System.Text;

namespace L2EmuLibs.Utils
{
    public class Logger
    {
        public Logger()
        {

        }

        public enum LogType
        {
            Debug,
            AI,
            Network,
            Error,
            Test,
            Initialize,
            None
        }

        public enum Destination
        {
            Server,
            Game
        }

        private static object Locky = new object();

        public static void WriteLog(string log, LogType type)
        {
            lock (Locky)
            {
                switch (type)
                {
                    case LogType.AI:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("[AI] " + log);
                        break;
                    case LogType.Debug:
#if DEBUG
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.WriteLine("[Debug] " + log);
#endif
                        break;
                    case LogType.Network:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("[Network] " + log);
                        break;
                    case LogType.Error:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("[Error] " + log);
                        break;
                    case LogType.Test:
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.WriteLine("[Test] " + log);
                        break;
                    case LogType.Initialize:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine("[Initialize] " + log);
                        break;
                    case LogType.None:
                        Console.WriteLine(log);
                        break;
                }
                Console.ResetColor();
            }
        }

        public static void WriteLog(Bytex message, Destination dest)
        {
            lock(Locky)
            {
                SortedList<string, string> values = message.GetObjectStrings(message);

                string TypeName = message.GetType().Name;
                Console.WriteLine(dest == Destination.Game ? "[Server->Game]" : "[Game->Server] " + message.GetType().Name);

                for (int i = 0; i < values.Count; i++)
                {
                    Console.WriteLine("\t" + values.Keys[i] + "\t => " + values.Values[i]);
                }
            }
        }
    }
}