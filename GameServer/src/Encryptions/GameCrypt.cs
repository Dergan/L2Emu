﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.src.Encryptions
{
    public class GameCrypt
    {
        private byte[] _inKey = new byte[16];
        private byte[] _outKey = new byte[16];
        private bool _isEnabled;

        public void setKey(byte[] key)
        {
            Array.Copy(key, 0, _inKey, 0, 16);
            Array.Copy(key, 0, _outKey, 0, 16);
        }

        public byte[] decrypt(byte[] raw, int offset, int size)
        {
            if (!_isEnabled)
                return raw;

            int temp = 0;
            for (int i = 0; i < size; i++)
            {
                int temp2 = raw[offset + i] & 0xFF;
                raw[offset + i] = (byte)(temp2 ^ _inKey[i & 15] ^ temp);
                temp = temp2;
            }
            Array.Copy(BitConverter.GetBytes(BitConverter.ToInt32(_inKey, 8) + size), 0, _inKey, 8, 4);
            return raw;
        }

        public byte[] encrypt(byte[] raw, int offset, int size)
        {
            if (!_isEnabled)
            {
                _isEnabled = true;
                return raw;
            }

            int temp = 0;
            for (int i = 0; i < size; i++)
            {
                int temp2 = raw[offset + i] & 0xFF;
                temp = temp2 ^ _outKey[i & 15] ^ temp;
                raw[offset + i] = (byte)temp;
            }
            Array.Copy(BitConverter.GetBytes(BitConverter.ToInt32(_outKey, 8) + size), 0, _outKey, 8, 4);
            return raw;
        }
    }
}
