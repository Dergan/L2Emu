﻿using L2EmuLibs.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.src
{
    public class Config
    {
        private static string General_Config_Path = "./Config/General.ini";

        static Config()
        {
            Logger.WriteLog("Loading Configuration files...", Logger.LogType.Initialize);

            ConfigFile general = new ConfigFile(General_Config_Path);
            LoginListenAddr = general.GetProperty("Network Login Server", "ConnectIP", "127.0.0.1");
            LoginListenPort = ushort.Parse(general.GetProperty("Network Login Server", "Port", "4001"));

            GameServerListenAddr = general.GetProperty("Network Game Server", "ListenIP", "0.0.0.0");
            GameServerExternalAddr = general.GetProperty("Network Game Server", "ExternalIP", "127.0.0.1");
            GameServerInternalAddr = general.GetProperty("Network Game Server", "InternalIp", "127.0.0.1");
            GameServerListenPort = ushort.Parse(general.GetProperty("Network Game Server", "ListenPort", "7777"));

            NpcserverListenAddr = general.GetProperty("Npc Server", "NpcListenIP", "0.0.0.0");
            NpcListenPort = short.Parse(general.GetProperty("Npc Server", "NpcListenPort", "4002"));

            PvP = bool.Parse(general.GetProperty("Network Game Server", "PVP", "False"));
            MaxPlayers = short.Parse(general.GetProperty("Network Game Server", "MaxPlayers", "10000"));
            ShowClock = bool.Parse(general.GetProperty("Network Game Server", "ShowClock", "False"));
            Brackets = bool.Parse(general.GetProperty("Network Game Server", "Brackets", "False"));
            ServerId = short.Parse(general.GetProperty("Network Game Server", "ServerId", "1"));
            ServerHash = general.GetProperty("Network Game Server", "ServerHash", "");

            IsDebugMode = bool.Parse(general.GetProperty("Debug", "IsDebugMode", "False"));

            DbConnectionString = general.GetProperty("Database", "ConnectionString", "DRIVER=SQLite3 ODBC Driver;Database=./Data/Loginserver.db;");

            MinProtocolVersion = short.Parse(general.GetProperty("Protocol Versions", "MinVersion", "215"));
            MaxProtocolVersion = short.Parse(general.GetProperty("Protocol Versions", "MaxVersion", "215"));
            ExternalIpAddress = general.GetProperty("Network Game Server", "ExternalIp", "127.0.0.1");
        }

        public static string LoginListenAddr
        {
            get;
            private set;
        }

        public static ushort LoginListenPort
        {
            get;
            private set;
        }

        public static string GameServerListenAddr
        {
            get;
            private set;
        }

        public static ushort GameServerListenPort
        {
            get;
            private set;
        }

        public static bool IsDebugMode
        {
            get;
            private set;
        }

        public static bool PvP
        {
            get;
            private set;
        }

        public static short MaxPlayers
        {
            get;
            private set;
        }

        public static bool ShowClock
        {
            get;
            private set;
        }

        public static bool Brackets
        {
            get;
            private set;
        }

        public static string GameServerExternalAddr
        {
            get;
            private set;
        }

        public static string GameServerInternalAddr
        {
            get;
            private set;
        }

        public static int ServerId
        {
            get;
            private set;
        }

        public static string DbConnectionString
        {
            get;
            private set;
        }

        public static short MinProtocolVersion
        {
            get;
            private set;
        }

        public static short MaxProtocolVersion
        {
            get;
            private set;
        }

        public static string ServerHash
        {
            get;
            private set;
        }

        public static string NpcserverListenAddr
        {
            get;
            private set;
        }

        public static short NpcListenPort
        {
            get;
            private set;
        }
        public static string ExternalIpAddress
        {
            get;
            private set;
        }
    }
}