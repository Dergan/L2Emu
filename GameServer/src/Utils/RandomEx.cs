﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.src.Utils
{
    public class RandomEx
    {
        private static Random rnd;

        static RandomEx()
        {
            rnd = new Random();
        }

        public static int Next()
        {
            lock (rnd)
            {
                return rnd.Next();
            }
        }

        public static int Next(int maxValue)
        {
            lock (rnd)
            {
                return rnd.Next(maxValue);
            }
        }

        public static int Next(int minValue, int maxValue)
        {
            lock (rnd)
            {
                return rnd.Next(minValue, maxValue);
            }
        }

        public static byte[] NextBytes(int Length)
        {
            lock (rnd)
            {
                byte[] ret = new byte[Length];
                rnd.NextBytes(ret);
                return ret;
            }
        }

        public static double NextDouble()
        {
            lock (rnd)
            {
                return rnd.NextDouble();
            }
        } 
    }
}
