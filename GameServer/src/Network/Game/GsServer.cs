﻿using L2EmuLibs.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.src.Network.Game
{
    public class GsServer : Server
    {
        public GsServer()
            : base(Config.GameServerListenAddr, Config.GameServerListenPort)
        {

        }

        public override Peer GetNewClient(System.Net.Sockets.Socket socket)
        {
            return new GamePeer(socket);
        }

        public override void onNewClient(Peer peer)
        {

        }
    }
}