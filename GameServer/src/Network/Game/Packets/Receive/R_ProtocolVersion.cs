﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.src.Network.Game.Packets.Receive
{
    public class R_ProtocolVersion : Bytex
    {
        [XPacketId(0x0E)]
        public byte PacketId { get; set; }

        [XInteger]
        public int GameVersion { get; set; }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }
    }
}