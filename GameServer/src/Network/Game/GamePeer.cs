﻿using GameServer.src.Encryptions;
using GameServer.src.Network.Game.Packets.Receive;
using GameServer.src.Utils;
using L2EmuLibs.ByteX;
using L2EmuLibs.Network;
using L2EmuLibs.Utils;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace GameServer.src.Network.Game
{
    public class GamePeer : Peer
    {
        private GameCrypt gameCrypt;
        private PacketHandler packetHandler;

        public GamePeer(Socket sock)
            : base(sock)
        {
            
        }

        protected override void onConnect()
        {
            this.gameCrypt = new GameCrypt();
            this.gameCrypt.setKey(GenerateKey());
            this.packetHandler = new PacketHandler(typeof(R_ProtocolVersion).Namespace);
        }

        protected override void onDisconnect()
        {

        }

        protected override void onReceivePayload(byte[] Data)
        {
            if (Data.Length == 0)
            {
                //data shouldn't be 0 bytes long
                Disconnect();
                return;
            }
            gameCrypt.decrypt(Data, 0, Data.Length);


            int packetId = Data[0];
            Bytex packet = packetHandler.GetPacket(packetId);
            if (packet != null)
            {
                packet.Deserialize(Data, packet);
                packet.Peer = this;

                Logger.WriteLog(packet, Logger.Destination.Server);

                packet.onReceive();
            }
            else
            {
                Disconnect(); //unknown packet
            }
        }

        protected override void onSendPacket(ref byte[] Payload, Bytex byteX)
        {
            Payload = gameCrypt.decrypt(Payload, 0, Payload.Length);
            Logger.WriteLog(byteX, Logger.Destination.Game);
        }

        private byte[] GenerateKey()
        {
            byte[] key = RandomEx.NextBytes(16);

            // the last 8 bytes are static
            key[8] = 0xC8;
            key[9] = 0x27;
            key[10] = 0x93;
            key[11] = 0x01;
            key[12] = 0xA1;
            key[13] = 0x6C;
            key[14] = 0x31;
            key[15] = 0x97;
            return key;
        }
    }
}
