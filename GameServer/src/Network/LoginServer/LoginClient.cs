﻿using SecureSocketProtocol2;
using SecureSocketProtocol2.Misc;
using SecureSocketProtocol2.Network;
using SecureSocketProtocol2.Network.Protections;
using SecureSocketProtocol2.Network.Protections.Compression;
using SecureSocketProtocol2.Network.RootSocket;
using SecureSocketProtocol2.Plugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GameServer.src.Network.LoginServer
{
    public class LoginClient : SSPClient
    {
        public LoginClient()
            : base(new ClientProperties(Config.LoginListenAddr, Config.LoginListenPort, typeof(LoginClientChannel), new object[0], new byte[]
            { //private key, can be any size you want
                80, 118, 131, 114, 195, 224, 157, 246, 141, 113,
                186, 243, 77, 151, 247, 84, 70, 172, 112, 115,
                112, 110, 91, 212, 159, 147, 180, 188, 143, 251,
                218, 155
            }, new Stream[] {/*key files*/ }, null, 30000,
            //login
               "Dergan", "Hunter:)"))
        {

        }

        public override void onClientConnect()
        {

        }

        public override void onDisconnect(DisconnectReason Reason)
        {

        }

        public override void onValidatingComplete()
        {
            Console.WriteLine("Validating connection...");
        }

        public override void onKeepAlive()
        {

        }

        public override void onException(Exception ex, ErrorType errorType)
        {
            Console.WriteLine(ex);
        }

        public override void onReconnect()
        {

        }

        public override void onNewChannelOpen(Channel channel)
        {

        }

        public override bool onVerifyCertificate(CertInfo certificate)
        {
            return true;
        }

        public override IPlugin[] onGetPlugins()
        {
            return new IPlugin[]
            {

            };
        }

        public override void onAddProtection(Protection protection)
        {
            protection.AddProtection(new QuickLzProtection());
        }

        public override uint HeaderJunkCount
        {
            get { return 5; }
        }

        public override uint PrivateKeyOffset
        {
            get { return 500; }
        }

        public override void onAuthenticated()
        {

        }

        public override void onNewStreamOpen(SecureStream stream)
        {

        }

        public override void onShareClasses()
        {

        }

        public override bool onPeerConnectionRequest(RootPeer peer)
        {
            return false;
        }

        public override RootPeer onGetNewPeerObject()
        {
            return null;
        }
    }
}
