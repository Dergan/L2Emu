﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using System.Data.Linq;
using System.Linq;

namespace LoginServer.LoginDatabase
{
    public class Database : IDisposable
    {
        private DataContext dataContext;

        public Database()
        {
            dataContext = new LoginDbLayoutDataContext("Server=DERGAN-PC\\SQLEXPRESS;Database=L2Emu_Login;Trusted_Connection=True;");
        }

        public bool LoginSuccess(string Username, string Password)
        {
           if (dataContext.GetTable<tblAccounts>().Where(o => o.Username.ToLower() == Username && o.Password == Password).ToArray().Length > 0)
            {
                return true;
            }
            return false;
        }

        public tblGameServer[] GetGameServers()
        {
            return dataContext.GetTable<tblGameServer>().ToArray();
        }

        public void Dispose()
        {

        }
    }
}