﻿using LoginServer.src.Network.GameServer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LoginServer
{
    class Program
    {
        public static LoginServer.src.Network.Login.LoginServer loginServer;
        public static GameServerListener gameServerListener;

        static void Main(string[] args)
        {
            loginServer = new src.Network.Login.LoginServer();
            //gameServerListener = new GameServerListener();

            Process.GetCurrentProcess().WaitForExit();
        }
    }
}