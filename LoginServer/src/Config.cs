﻿using L2EmuLibs.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace LoginServer.src
{
    public class Config
    {
        private static string General_Config_Path = "./Config/General.ini";

        static Config()
        {
            Logger.WriteLog("Loading Configuration files...", Logger.LogType.Initialize);

            ConfigFile general = new ConfigFile(General_Config_Path);
            ClientListenAddr = general.GetProperty("Network Client", "ListenIP", "0.0.0.0");
            ClientListenPort = short.Parse(general.GetProperty("Network Client", "Port", "2106"));
            GameServerListenAddr = general.GetProperty("Network Gameserver", "ListenIP", "0.0.0.0");
            GameServerListenPort = ushort.Parse(general.GetProperty("Network Gameserver", "Port", "4001"));
            DbConnectionString = general.GetProperty("Database", "ConnectionString", "DRIVER=SQLite3 ODBC Driver;Database=./Data/Loginserver.db;");
            ShowAgreement = bool.Parse(general.GetProperty("Misc", "ShowAgreement", "false"));
            AutoCreateAccount = bool.Parse(general.GetProperty("Misc", "AutoCreateAccount", "true"));
            IsDebugMode = bool.Parse(general.GetProperty("Debug", "IsDebugMode", "false"));
        }

        public static string ClientListenAddr
        {
            get;
            private set;
        }

        public static short ClientListenPort
        {
            get;
            private set;
        }

        public static string GameServerListenAddr
        {
            get;
            private set;
        }

        public static ushort GameServerListenPort
        {
            get;
            private set;
        }

        public static bool IsDebugMode
        {
            get;
            private set;
        }

        public static string DbConnectionString
        {
            get;
            private set;
        }

        public static bool ShowAgreement
        {
            get;
            private set;
        }

        public static bool AutoCreateAccount
        {
            get;
            private set;
        }
    }
}
