﻿using SecureSocketProtocol2;
using SecureSocketProtocol2.Misc;
using SecureSocketProtocol2.Network;
using SecureSocketProtocol2.Network.Protections;
using SecureSocketProtocol2.Network.Protections.Compression;
using SecureSocketProtocol2.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.GameServer
{
    public class GsPeer : SSPClient
    {
        public GsPeer()
            : base(typeof(PeerChannel), null, false)
        {

        }

        public override void onClientConnect()
        {

        }

        public override void onValidatingComplete()
        {
            Console.WriteLine("Validating connection...");
        }

        public override void onDisconnect(DisconnectReason Reason)
        {
            Console.WriteLine("Client disconnected");
        }

        public override void onKeepAlive()
        {
            Console.WriteLine("Received keep-alive");
        }

        public override void onException(Exception ex, ErrorType errorType)
        {

        }

        public override void onReconnect()
        {

        }

        public override void onNewChannelOpen(Channel channel)
        {

        }

        public override bool onVerifyCertificate(CertInfo certificate)
        {
            return true;
        }

        public override IPlugin[] onGetPlugins()
        {
            return new IPlugin[]
            {

            };
        }

        public override void onAddProtection(Protection protection)
        {
            protection.AddProtection(new QuickLzProtection());
        }

        public override uint HeaderJunkCount
        {
            get { return 5; }
        }
        public override uint PrivateKeyOffset
        {
            get { return 500; }
        }

        public override void onAuthenticated()
        {

        }

        public override void onNewStreamOpen(SecureStream stream)
        {

        }

        public override void onShareClasses()
        {
            //base.ShareClass("SharedTest", typeof(SharedTest));
        }

        public override bool onPeerConnectionRequest(SecureSocketProtocol2.Network.RootSocket.RootPeer peer)
        {
            //should never happen at server side
            Console.WriteLine("onPeerConnectionRequest got executed which should never happen... strange");
            return false;
        }

        public override SecureSocketProtocol2.Network.RootSocket.RootPeer onGetNewPeerObject()
        {
            return null;
        }
    }
}