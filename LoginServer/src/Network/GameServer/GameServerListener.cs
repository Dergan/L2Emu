﻿using L2EmuLibs.Utils;
using SecureSocketProtocol2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.GameServer
{
    public class GameServerListener : SSPServer
    {
        public GameServerListener()
            : base(new ServerProps())
        {
            Logger.WriteLog("Game Server Listener is running...", Logger.LogType.Network);
        }

        public override bool onAuthentication(SSPClient client, string Username, string Password)
        {
            return true;
        }

        public override void onConnectionAccept(SSPClient client)
        {

        }

        public override void onConnectionClosed(SSPClient client)
        {

        }

        public override void onException(Exception ex)
        {

        }

        public override bool onPeerConnectionRequest(SSPClient FromClient, SSPClient ToClient)
        {
            return false;
        }

        public override bool onPeerCreateDnsRequest(string DnsName, SSPClient Requestor)
        {
            return false;
        }
    }

    public class ServerProps : ServerProperties
    {
        public ServerProps()
            : base()
        {

        }

        public override ushort ListenPort { get { return Config.GameServerListenPort; } }
        public override string ListenIp { get { return Config.GameServerListenAddr; } }
        public override bool AllowUdp { get { return false; } }
        public override bool UserPassAuthenication { get { return true; } }

        public override CertificateInfo ServerCertificate
        {
            get { return new Certificate(); }
        }

        public override Stream[] KeyFiles
        {
            get
            {
                return new Stream[]
                {

                };
            }
        }

        public override bool GenerateKeysInBackground
        {
            get { return false; }
        }

        public override SSPClient GetNewClient()
        {
            return new GsPeer();
        }
    }

    public class Certificate : CertificateInfo
    {
        private DateTime CreatedAt;

        public override string CommonName
        {
            get { return "Secure Socket Protocol"; }
        }

        public override string Country
        {
            get { return "The Netherlands"; }
        }

        public override string State
        {
            get { return "Unknown"; }
        }

        public override string Locality
        {
            get { return "Unknown"; }
        }

        public override DateTime ValidTo
        {
            get { return this.CreatedAt.AddYears(5); }
        }

        public override DateTime ValidFrom
        {
            get { return this.CreatedAt; }
        }

        public override string Organization
        {
            get { return "DragonHunter's Cave"; }
        }

        public override string Unit
        {
            get { return "Unknown"; }
        }

        public override string IssuerCommonName
        {
            get { return "Unknown"; }
        }

        public override string IssuerOrganization
        {
            get { return "Unknown"; }
        }

        public override string IssuerCountry
        {
            get { return "Unknown"; }
        }

        public override bool ShowProtectionMethods
        {
            get { return false; }
        }

        public override ChecksumHash Checksum
        {
            get { return ChecksumHash.SHA512; }
        }

        public override byte[] PrivateKey
        {
            get
            {
                return new byte[]
                {
                    80, 118, 131, 114, 195, 224, 157, 246, 141, 113,
                    186, 243, 77, 151, 247, 84, 70, 172, 112, 115,
                    112, 110, 91, 212, 159, 147, 180, 188, 143, 251,
                    218, 155
                };
            }
        }

        public Certificate()
            : base()
        {
            this.CreatedAt = DateTime.Now;
        }
    }
}