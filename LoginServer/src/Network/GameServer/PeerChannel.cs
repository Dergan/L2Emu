﻿using SecureSocketProtocol2.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.GameServer
{
    public class PeerChannel : Channel
    {
        public PeerChannel()
        {

        }

        public override void onChannelClosed()
        {

        }

        public override void onChannelOpen()
        {

        }

        public override void onDeepPacketInspection(SecureSocketProtocol2.Network.Messages.IMessage message)
        {

        }

        public override void onReceiveMessage(SecureSocketProtocol2.Network.Messages.IMessage message)
        {

        }
    }
}