﻿using L2EmuLibs.Network;
using L2EmuLibs.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoginServer.src.Network.Login
{
    public class LoginServer : Server
    {
        public LoginServer()
            : base(Config.ClientListenAddr, Config.ClientListenPort)
        {
            Logger.WriteLog("Login Server is running...", Logger.LogType.Network);
        }

        public override Peer GetNewClient(System.Net.Sockets.Socket socket)
        {
            return new LoginPeer(socket);
        }

        public override void onNewClient(Peer peer)
        {

        }
    }
}