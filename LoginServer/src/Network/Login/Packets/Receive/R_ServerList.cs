﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using LoginServer.src.Network.Login.Packets.Send;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Receive
{
    public class R_ServerList : Bytex
    {
        [XPacketId(0x05)]
        public byte PacketId { get; set; }

        [XInteger]
        public int LoginOk_1 { get; set; }

        [XInteger]
        public int LoginOk_2 { get; set; }

        public R_ServerList()
        {

        }

        public override void onReceive()
        {
            Peer.SendPacket(new S_ServerList());
        }

        public override void onSend()
        {

        }
    }
}