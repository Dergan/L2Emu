﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using LoginServer.LoginDatabase;
using LoginServer.src.Network.Login.Packets.Send;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Receive
{
    public class R_AuthLogin : Bytex
    {
        [XPacketId(0)]
        public byte PacketId { get; set; }

        [XByteArrayAttribute(true)]
        public byte[] Buffer { get; set; }



        public override void onReceive()
        {
            LoginPeer loginPeer = (LoginPeer)base.Peer;

            string username = "";
            string password = "";
            CipherParameters key = loginPeer.ScrambledPair._privateKey;
            RSAEngine rsa = new RSAEngine();
            rsa.init(false, key);

            byte[] decrypt = rsa.processBlock(Buffer, 0, 128);

            if (decrypt.Length <= 128)
            {
                byte[] temp = new byte[128];
                Array.Copy(decrypt, 0, temp, 128 - decrypt.Length, decrypt.Length);
                decrypt = temp;
            }

            username = PrepareString(Encoding.ASCII.GetString(decrypt, 0x5E, 14).ToLower());
            password = PrepareString(Encoding.ASCII.GetString(decrypt, 0x6C, 16));

            string HashedPass = BitConverter.ToString(MD5.Create().ComputeHash(ASCIIEncoding.ASCII.GetBytes(password))).Replace("-", "");

            using(Database db = new Database())
            {
                if (db.LoginSuccess(username, HashedPass))
                {
                    Peer.SendPacket(new S_LoginOk());
                }
                else
                {
                    Peer.SendPacket(new S_LoginFail(S_LoginFail.FailReason.USER_OR_PASS_WRONG));
                }
            }
        }

        public override void onSend()
        {

        }

        private string PrepareString(string Value)
        {
            string newStr = "";
            for (short i = 0; i < Value.Length - 1; i++)
            {
                if (char.IsLetterOrDigit(Value[i]))
                    newStr += Value[i];
            }
            return newStr;
        }
    }
}