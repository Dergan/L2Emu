﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using LoginServer.src.Network.Login.Packets.Send;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Receive
{
    public class R_AuthGameGuard : Bytex
    {
        [XPacketId(7)]
        public byte PacketId { get; set; }

        [XInteger]
        public int SessionId { get; set; }

        public R_AuthGameGuard()
        {

        }

        public override void onReceive()
        {
            Peer.SendPacket(new S_AuthGG());
        }

        public override void onSend()
        {

        }
    }
}
