﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using LoginServer.src.Network.Login.Packets.Send;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Receive
{
    public class R_Login : Bytex
    {
        [XPacketId(2)]
        public byte PacketId { get; set; }

        public R_Login()
            : base()
        {

        }

        public override void onReceive()
        {
            Peer.SendPacket(new S_PlayOK());
        }

        public override void onSend()
        {

        }
    }
}
