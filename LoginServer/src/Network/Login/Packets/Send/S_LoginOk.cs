﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Send
{
    public class S_LoginOk : Bytex
    {
        [XPacketId(0x03)]
        public byte PacketId { get; set; }

        [XInteger(1)]
        public int LoginOk_1 { get; set; }

        [XInteger(2)]
        public int LoginOk_2 { get; set; }

        [XInteger(0)]
        public int Unknown_1 { get; set; }

        [XInteger(0)]
        public int Unknown_2 { get; set; }

        [XInteger(0x000003ea)]
        public int Unknown_3 { get; set; }

        [XByteArray(new byte[28] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, })]
        public int Unknown_4 { get; set; }

        public S_LoginOk()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }
    }
}