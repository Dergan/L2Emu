﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using L2EmuLibs.Utils;
using LoginServer.LoginDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Send
{
    public class S_ServerList : Bytex
    {
        [XPacketId(0x04)]
        public byte PacketId { get; set; }

        [XByte]
        public byte ServerCount { get { return (byte)ServerList.Count; } set { } }

        [XByte]
        public byte LastServerIndex { get; set; }

        [XList]
        public List<ServerListItem> ServerList { get; set; }

        public S_ServerList()
        {
            this.ServerList = new List<ServerListItem>();
        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {
            using (Database db = new Database())
            {
                foreach (tblGameServer gameServer in db.GetGameServers())
                {
                    ServerList.Add(new ServerListItem(gameServer));
                }
            }
        }

        [XNoPacketIdAttribute]
        public class ServerListItem : Bytex
        {
            [XByte]
            public byte ServerId { get; set; }

            [XByteArray(4)]
            public byte[] AddressBytes { get; set; }

            [XInteger]
            public int Port { get; set; }

            [XByte]
            public byte AgeLimit { get; set; }

            [XByte]
            public byte isPvpServer { get; set; }

            [XShort]
            public short PlayerCount { get; set; }

            [XShort]
            public short MaxPlayerCount { get; set; }

            [XByte]
            public byte isOnline { get; set; }

            [XInteger]
            public int ShowClock { get; set; }

            [XByte]
            public byte ServerBrackets { get; set; }

            public ServerListItem(tblGameServer gameServer)
            {
                ServerId = 1;
                AddressBytes = new byte[] { 127, 0, 0, 1 };
                Port = 7777;
                AgeLimit = 0;
                isPvpServer = 0;
                PlayerCount = 0;
                MaxPlayerCount = 10000;
                isOnline = 1;
                ShowClock = 0;
                ServerBrackets = 0;

                /*if (NetUtils.CheckIsLocal(Client.RemoteEndPoint.Address.GetAddressBytes()))
                    WriteBytes(ServerList.Instance.GameServerList.Values[i].InternalIP.GetAddressBytes());
                else
                    WriteBytes(ServerList.Instance.GameServerList.Values[i].ExternalIP.GetAddressBytes());*/
            }

            public override void onReceive()
            {

            }

            public override void onSend()
            {

            }
        }
    }
}