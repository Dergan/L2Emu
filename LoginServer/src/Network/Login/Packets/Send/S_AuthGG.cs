﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Send
{
    public class S_AuthGG : Bytex
    {
        [XPacketId(0x0B)]
        public byte PacketId { get; set; }

        [XInteger(0)]
        public int SessionId { get; set; }

        [XInteger(0)]
        public int Unknown_1 { get; set; }

        [XInteger(0)]
        public int Unknown_2 { get; set; }

        [XInteger(0)]
        public int Unknown_3 { get; set; }

        [XInteger(0)]
        public int Unknown_4 { get; set; }

        public S_AuthGG()
            : base()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }
    }
}