﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Send
{
    public class S_LoginFail : Bytex
    {
        [XPacketId(0x01)]
        public byte PacketId { get; set; }

        [XInteger]
        public int Reason { get; set; }

        public S_LoginFail(FailReason reason)
            : base()
        {
            this.Reason = (int)reason;
        }

        public enum FailReason
        {
            NOTHING = 0,
            SYSTEM_ERROR_TRY_AGAIN = 1,
            PASS_WRONG = 2,
            USER_OR_PASS_WRONG = 3,
            ACCESS_FAILED_TRY_AGAIN = 4,
            INCORRECT_ACCOUNT_INFO = 5,
            ACCOUNT_IN_USE = 7,
            TOO_YOUNG = 12,
            SERVER_OVERLOADED = 15,
            SERVER_MAINTENANCE = 16,
            CHANGE_TEMP_PASS = 17,
            TEMP_PASS_EXPIRED = 18,
            NO_TIME_LEFT = 19,
            SYSTEM_ERROR = 20,
            ACCESS_FAILED = 21,
            RESTRICTED_IP = 22,
            SIX = 25, //Just the number 6 LOL!30 = WEEK_TIME_FINISHED
            SECURITY_CARD = 31, //Enter Security card number
            NOT_VERIFY_AGE = 32,
            NO_ACCESS_COUPON = 33,
            DUAL_BOX = 35,
            INACTIVE_REACTIVATE = 36,
            ACCEPT_USER_AGREEMENT = 37,
            GUARDIAN_CONSENT = 38,
            DECLINED_AGREEMENT_OR_WIDTHDRAWL = 39,
            ACCOUNT_SUSPENDED = 40,
            CHANGE_PASS_QUIZ = 41,
            ACCESSED_10_ACCOUNTS = 42,
            MASTER_ACCOUNT_RESTRICTED = 43,
            CERTIFICATION_FAILED = 46,
            PHONE_SERVICE_OFFLINE = 47,
            PHONE_SIGNAL_DELAY = 48,
            PHONE_CALL_NOT_RECEIVED = 49,
            PHONE_EXPIRED = 50,
            PHONE_CHECKED = 51,
            PHONE_HEAVY_VALUME = 52,
            PHONE_EXPIRED_BLOCKED = 53,
            PHONE_FAILED_3_TIMES = 54,
            MAX_PHONE_USES_EXCEEDED = 55,
            PHONE_UNDERWAY = 56
        };

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }
    }
}
