﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using LoginServer.src.Network.Login.Crypt;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.send
{
    public class S_Init : Bytex
    {
        [XPacketId(0)]
        public byte PacketId { get; set; }

        [XInteger(0)]
        public int SessionId { get; set; }

        [XInteger(0xc621)]
        public int ProtocolVersion { get; set; }

        [XByteArray(128)]
        public byte[] RSAPublicKey { get; set; }

        [XInteger(0x29DD954E)]
        public int GameGuardChecksum_1 { get; set; }

        [XInteger(0x77C39CFC)]
        public int GameGuardChecksum_2 { get; set; }

        [XInteger(unchecked((int)0x97ADB620))]
        public int GameGuardChecksum_3 { get; set; }

        [XInteger(0x07BDE0F7)]
        public int GameGuardChecksum_4 { get; set; }

        [XByteArray(16)]
        public byte[] BlowfishKey { get; set; }

        [XByte(0)]
        public byte NullTermination { get; set; }

        public S_Init(ScrambledKeyPair scrambledPair, byte[] blowfishKey)
        {
            this.RSAPublicKey = scrambledPair._scrambledModulus;
            this.BlowfishKey = blowfishKey;
        }

        public S_Init()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }
    }
}