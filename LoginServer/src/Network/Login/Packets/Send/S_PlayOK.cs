﻿using L2EmuLibs.ByteX;
using L2EmuLibs.ByteX.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginServer.src.Network.Login.Packets.Send
{
    public class S_PlayOK : Bytex
    {
        [XPacketId(7)]
        public byte PacketId { get; set; }

        [XInteger(1)]
        public byte PlayOk1 { get; set; }

        [XInteger(2)]
        public byte PlayOk2 { get; set; }

        public S_PlayOK()
            : base()
        {

        }

        public override void onReceive()
        {

        }

        public override void onSend()
        {

        }
    }
}