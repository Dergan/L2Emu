﻿using L2EmuLibs.ByteX;
using L2EmuLibs.Network;
using L2EmuLibs.Utils;
using LoginServer.src.Network.Login.Crypt;
using LoginServer.src.Network.Login.Packets;
using LoginServer.src.Network.Login.Packets.Receive;
using LoginServer.src.Network.Login.Packets.send;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace LoginServer.src.Network.Login
{
    public class LoginPeer : Peer
    {
        public byte[] BlowfishKey;
        public LoginCrypt LoginCrypt;
        public ScrambledKeyPair ScrambledPair;
        public PacketHandler packetHandler;

        public LoginPeer(Socket socket)
            : base(socket)
        {
            this.packetHandler = new PacketHandler(typeof(R_AuthGameGuard).Namespace);
        }

        protected override void onReceivePayload(byte[] Data)
        {
            if (!LoginCrypt.Decrypt(Data))
            {
                Disconnect();
                Logger.WriteLog("Wrong checksum used by client " + base.RemoteIp, Logger.LogType.Network);
            }

            if (Data.Length == 0)
            {
                //data shouldn't be 0 bytes long
                Disconnect();
                return;
            }

            int packetId = Data[0];
            Bytex packet = packetHandler.GetPacket(packetId);
            if (packet != null)
            {
                packet.Deserialize(Data, packet);
                packet.Peer = this;
                Logger.WriteLog(packet, Logger.Destination.Server);
                packet.onReceive();
            }
            else
            {
                Disconnect();//unknown packet
            }
        }

        protected override void onDisconnect()
        {

        }

        protected override void onConnect()
        {
            BlowfishKey = GenerateBlowfishKey();
            ScrambledPair = new ScrambledKeyPair(ScrambledKeyPair.genKeyPair());
            LoginCrypt = new LoginCrypt();
            LoginCrypt.updateKey(BlowfishKey);
            base.SendPacket(new S_Init(ScrambledPair, BlowfishKey));
        }

        private byte[] GenerateBlowfishKey()
        {
            byte[] temp = new byte[16];
            new Random(DateTime.Now.Millisecond).NextBytes(temp);
            return temp;
        }

        protected override void onSendPacket(ref byte[] Payload, Bytex byteX)
        {
            Payload = LoginCrypt.Encrypt(Payload);
            Logger.WriteLog(byteX, Logger.Destination.Game);
        }
    }
}